﻿using HotChocolate;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class Queries
    {
        public Task<IEnumerable<Role>> GetRolesAsync([Service]IRepository<Role> roles) => roles.GetAllAsync();
    }
}
