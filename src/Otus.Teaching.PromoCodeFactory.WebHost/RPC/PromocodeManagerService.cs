﻿using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Grpc;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.RPC
{
    public class PromocodeManagerService : PromocodeManager.PromocodeManagerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromocodeManagerService(IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }

        /// <summary>
        /// Получить все промокоды.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<GetPromocodesResponse> GetPromocodes(Empty request, ServerCallContext context)
        {
            var preferences = await _promoCodesRepository.GetAllAsync();

            var response = preferences.Select(x => new PromoCodeShortResponse
            {
                Id = x.Id.ToString(),
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            var result = new GetPromocodesResponse();
            result.PromoCodes.AddRange(response);

            return result;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<OperationResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request, ServerCallContext context)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetFirstWhere(x => x.Name == request.Preference);

            if (preference == null)
            {
                return new OperationResult { Result = OperationResult.Types.Result.Fail };
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapGromModel(request, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);

            return new OperationResult { Result = OperationResult.Types.Result.Success };
        }
    }
}
